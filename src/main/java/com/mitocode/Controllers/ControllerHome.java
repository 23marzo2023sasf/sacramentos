package com.mitocode.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ControllerHome {
	
	@GetMapping("/")
    public String home(Model model) {
        model.addAttribute("mensaje", "¡Hola desde Thymeleaf!");
        return "sacramento/listadoSacramento";
    }

}