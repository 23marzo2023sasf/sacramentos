package com.mitocode.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.Class.Usuario;
import com.mitocode.Models.Response;
import com.mitocode.Services.UsuarioServices;

@RestController
public class UsuarioController {

    @Autowired
    private UsuarioServices usuarioService;

    @PostMapping("/usuarios")
    public Response<Usuario> registrarUsuario(@RequestBody Usuario usuario) {
        // Lógica para registrar el usuario
        boolean exito = usuarioService.registrarUsuario(usuario);

        if (exito) {
            return new Response<>(HttpStatus.OK.value(), "Usuario registrado exitosamente", usuario);
        } else {
            return new Response<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error al registrar usuario", null);
        }
    }
}
