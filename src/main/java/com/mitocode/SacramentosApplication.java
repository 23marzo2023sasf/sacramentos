package com.mitocode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SacramentosApplication {

	public static void main(String[] args) {
		SpringApplication.run(SacramentosApplication.class, args);
	}

}
